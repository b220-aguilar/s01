name = "Batman"
age = 40
occupation = "philanthropist"
movie = "Avengers: End Game"
rating = 99.6

print(f"I am {name}, and I am {age} years old. I work as a {occupation}, and my rating for {movie} is {rating}%")


num1 = 1
num2 = 2
num3 = 3


print(num1 * num2)

print(num1 < num3)

print(num3 + num2)