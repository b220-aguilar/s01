import math

# ctrl + / - shortcut for comments in pythonL
# instead of double /, python uses 'hash' sign (#): 1 liner
"""
although we have no keybinda for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""
# print is similar to console.log in node/JS since it lets us print messages in the console/terminal
print("Hello World")	

# [Section] Variables
# we dont have to use keywords in python to set our variables
#  snake case is the convention in terms of naming the variables in python. it uses underscore (_) in between words and uses lowercasing

age=35
middle_initial="C"
print(age)
print(middle_initial)

# declaring and assigning values to multiple variables simultaneously is possible in python
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name1)
print(name2)
print(name3)
print(name4)

# [Section] Data Types
#string
full_name = "John Doe"
secret_code = 'Pa$$word'

#number
num_of_days = 365 #integer
pi_approx = 3.1416 #float
complex_num = 1+5j #complex 
print(complex_num)

#boolean
is_learning = True	#Capital 'T'
is_admin = False	#Capital 'F'


print("Hi! My name is " + full_name)

# [Section] f strings
# similar to JS template literals

print(f"Hi! My name is {full_name} and my age is {age}.")

# Typecasting
# is python's way of converting one data type into another, since does not have a type coercion like in JS
# print("My age is " + age) - would result in an error, due to "age" being an integer


# from int to string
print("My age is " + str(age))

# from string to int
print(age + int("9876"))

# integer is different from float in python
print(age + int(98.87))


# [Section] Operators
print(2 + 10) # addition
print(2 - 10) # subtraction
print(2 * 10) # multiplication
print(2 / 10) # division
print(2 % 10) # modulo (remainder)
print(2 ** 10) # exponential
print(math.sqrt(81))
# this is possible when we import "math" module

# [Section] Assignment Operators
num1 = 3
print(num1)
num1 = num1 + 4
print(num1)
num1 = num1 - 4
print(num1)
num1 = num1 * 4
print(num1)
num1 = num1 / 4
print(num1)
num1 = num1 % 4
print(num1)
num1 = num1 ** 4
print(num1)


# [Section] Comparison Operators
# returns boolean value
print(1 == 1)
print(1 == "1")
print(1 != 1)
print(1 > 1)
print(1 < 1)
print(1 >= 1)
print(1 <= 1)

print("----")



# [Section] Logical Operators
print(True and False)
print(True or False)
print(not False)